package bright.demo.service.impl;


import bright.demo.service.JobService;
import bright.demo.service.utils.JobServiceUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Amirhossein Mehdipour on 8/19/19
 */
@Service
@Slf4j
public class JobServiceImpl implements JobService {
	private static final String tempCron = 	"0 * * 1/1 * ? *";
	public static final long EXECUTION_TIME = 5000L;
	private final JobServiceUtils utils = new JobServiceUtils();
	private AtomicInteger count = new AtomicInteger();


	public void executeSampleJob() {

		log.info("The sample job has begun...");
		try {
			Thread.sleep(EXECUTION_TIME);
		} catch (InterruptedException e) {
			log.error("Error while executing sample job", e);
		} finally {
			count.incrementAndGet();
			log.info("Sample job has finished...");
		}
	}

	public int getNumberOfInvocations() {
		return count.get();
	}

	@Override
	public void test(String name) {
		JobDetail jobDetail = utils.makeJobDetail(name);
		CronTrigger cronTrigger = utils.makeCronTrigger(tempCron);
	}
}

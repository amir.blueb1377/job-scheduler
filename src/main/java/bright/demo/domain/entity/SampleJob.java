package bright.demo.domain.entity;

import bright.demo.service.impl.JobServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Amirhossein Mehdipour on 8/19/19
 */
@Component
@Slf4j
public class SampleJob implements Job {
	@Autowired
	private JobServiceImpl jobServiceImpl;

	public void execute(JobExecutionContext context) throws JobExecutionException {

		log.info("Job ** {} ** fired @ {}", context.getJobDetail().getKey().getName(), context.getFireTime());

		jobServiceImpl.executeSampleJob();

		log.info("Next job scheduled @ {}", context.getNextFireTime());

	}
}

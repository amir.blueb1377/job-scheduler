package bright.demo.controller;

import bright.demo.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Amirhossein Mehdipour on 8/19/19
 */
@RestController
@RequestMapping("/job-schedule")
public class JobScheduleController {

	@Autowired
	private final JobService jobService;

	public JobScheduleController(JobService jobService) {
		this.jobService = jobService;
	}

	@GetMapping("/test")
	public ResponseEntity test(@RequestParam(value = "name", required = false) String name){
		jobService.test(name);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
